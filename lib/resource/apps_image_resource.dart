// resource image apps

class AppsImageResource {
  AppsImageResource._();
  static const String base = "assets/image";
  static const String login = "$base/login.png";
}
