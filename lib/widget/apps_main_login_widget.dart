// apps formfield login widget

// ignore_for_file: deprecated_member_use

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

class AppMainLoginWidget extends StatefulWidget {
  const AppMainLoginWidget({super.key});

  @override
  State<AppMainLoginWidget> createState() => _AppMainLoginWidgetState();
}

class _AppMainLoginWidgetState extends State<AppMainLoginWidget> {
  @override
  Widget build(BuildContext context) {
    return Form(
        key: AppsPublicVariableResource.formkey,
        child: Column(children: [
          AppsPublicVariableResource.jarakheight32,
          AppsTextFieldWidget(
              controller: AppsPublicVariableResource.usernameController,
              jenisField: JenisField.universal,
              label: "Username",
              hintText: "username",
              namaField: "username",
              maxLines: 1,
              keyboardType: TextInputType.text,
              suffixIcon: Icon(
                Icons.person,
                color: Theme.of(context).primaryColor,
              ),
              onChanged: (value) {},
              onsaved: (value) {
                AppsPublicVariableResource.username = value;
              }),
          if (AppsPublicVariableResource.username != "irfanda" &&
              AppsPublicVariableResource.username != "")
            appsCustomMessageError(context, ErrorFor.username),
          AppsPublicVariableResource.jarakheight24,
          AppsTextFieldWidget(
              controller: AppsPublicVariableResource.passwordController,
              jenisField: JenisField.password,
              label: "Password",
              hintText: "password",
              namaField: "Password",
              maxLines: 1,
              keyboardType: TextInputType.number,
              suffixIcon: IconButton(
                icon: (AppsPublicVariableResource.obscureText)
                    ? Icon(
                        Icons.visibility_off,
                        color: Theme.of(context).primaryColor,
                      )
                    : Icon(
                        Icons.visibility,
                        color: Theme.of(context).primaryColor,
                      ),
                onPressed: () {
                  setState(() {
                    AppsPublicVariableResource.obscureText =
                        !AppsPublicVariableResource.obscureText;
                  });
                },
              ),
              onChanged: (value) {},
              onsaved: (value) {
                AppsPublicVariableResource.password = value;
              }),
          if (AppsPublicVariableResource.password != "1234" &&
              AppsPublicVariableResource.password != "")
            appsCustomMessageError(context, ErrorFor.password),
          AppsPublicVariableResource.jarakheight32,
          AppsButtonWidget(
              buttonType: ButtonType.normalButton,
              titleButton: "Submit",
              backgroundColorButton: Theme.of(context).primaryColor,
              navigatorButton: () {
                setState(() {});
                appsLoginFunction(setState, context);
              }),
        ]));
  }
}
