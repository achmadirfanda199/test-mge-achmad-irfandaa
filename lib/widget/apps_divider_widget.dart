// apps divider

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

Widget basicDivider(BuildContext context) {
  return const Divider(color: Colors.grey);
}

Widget customDivider(BuildContext context, colorCustom) {
  return Divider(color: colorCustom);
}
