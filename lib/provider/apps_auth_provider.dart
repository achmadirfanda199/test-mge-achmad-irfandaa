// apps user provider

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

class AppsAuthProvider {
  String username;
  String password;
  bool isLogin;

  AppsAuthProvider({
    required this.username,
    required this.password,
    this.isLogin = false,
  });
}

class AppsAuthNotifier extends ChangeNotifier {
  late AppsAuthProvider appsauthdataprovider;
  AppsAuthNotifier() {
    appsauthdataprovider = AppsAuthProvider(
      username: "",
      password: "",
      isLogin: false,
    );
    loadUserData();
    notifyListeners();
  }

  AppsAuthProvider get appsUserDataProvider => appsauthdataprovider;

  // save data if user login to home page
  void saveUserLogin(AppsAuthProvider appsAuthProvider) async {
    final SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString("username", appsAuthProvider.username);
    sp.setString("password", appsAuthProvider.password);
    sp.setBool("isLogin", true);

    appsauthdataprovider = appsAuthProvider;
    notifyListeners();
  }

// load data if user come back to apps (or apps closed)
  void loadUserData() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    appsauthdataprovider = AppsAuthProvider(
      username: sp.getString("username") ?? "username",
      password: sp.getString("password") ?? "password",
      isLogin: sp.getBool("isLogin") ?? true,
    );
    notifyListeners();
  }

  // user log out
  void userLogOut() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString("username", "");
    sp.setString("passowrd", "");
    sp.setBool("isLogin", false);
    appsauthdataprovider;
    notifyListeners();
  }
}
