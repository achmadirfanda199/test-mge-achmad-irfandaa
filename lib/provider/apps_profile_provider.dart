//apps provider profile

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

class AppsProfileProvider {
  String usernameprofil;
  String phoneprofil;
  String addressprofil;
  String gambar;

  AppsProfileProvider({
    required this.usernameprofil,
    required this.phoneprofil,
    required this.addressprofil,
    required this.gambar,
  });
}

class AppsProfilNotifier extends ChangeNotifier {
  late AppsProfileProvider appsProfileProvider;
  AppsProfilNotifier() {
    appsProfileProvider = AppsProfileProvider(
      usernameprofil: "",
      phoneprofil: "",
      addressprofil: "",
      gambar: "",
    );
    notifyListeners();
  }

  // get data form login page
  void saveDataProfile(AppsProfileProvider appsProfileProvider) async {
    final SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString("usernameprofil", appsProfileProvider.usernameprofil);
    sp.setString("phoneprofil", appsProfileProvider.phoneprofil);
    sp.setString("addressprofil", appsProfileProvider.addressprofil);
    sp.setString("image", appsProfileProvider.gambar);

    appsProfileProvider = appsProfileProvider;
    notifyListeners();
  }
}
