// apps public variable

import 'dart:io';

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

class AppsPublicVariableResource {
  //jarak
  static Widget jarakheight8 = const SizedBox(
    height: 8,
  );
  static Widget jarakheight16 = const SizedBox(
    height: 16,
  );
  static Widget jarakheight24 = const SizedBox(
    height: 24,
  );
  static Widget jarakheight32 = const SizedBox(
    height: 32,
  );

  //width
  static Widget jarakwidth8 = const SizedBox(
    width: 8,
  );
  static Widget jarakwidth16 = const SizedBox(
    width: 16,
  );
  static Widget jarakwidth24 = const SizedBox(
    width: 24,
  );
  static Widget jarakwidth32 = const SizedBox(
    width: 32,
  );

  // loading get data empty
  static String getData = "";
  static String pesanErrorApi = ""; 

  // public key variable
  static GlobalKey<FormState> formkey = GlobalKey<FormState>();

  // user variable login
  static String username = "";
  static String password = "";

  // user variable crud data
  static String usernameprofil = "";
  static String phoneprofil = "";
  static String addressprofil = "";

  //login
  static TextEditingController usernameController = TextEditingController();
  static TextEditingController passwordController = TextEditingController();

  //profil
  static bool isLoad = false;
  static TextEditingController usernameControllerProfil =
      TextEditingController();
  static TextEditingController phoneControllerProfil = TextEditingController();
  static TextEditingController addressControllerProfil =
      TextEditingController();

  // message error
  static bool showerrorUsername = false;
  static bool showerrorPassword = false;
  static String messageErrorUsername = "";
  static String messageErrorPassword = "";

  //obscure textvalue passowrd
  static bool obscureText = true;

  // propfil user
  static File? gambarfile;
}
