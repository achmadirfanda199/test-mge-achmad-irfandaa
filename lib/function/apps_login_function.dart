// apps login page function
import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

void appsLoginFunction(
  setState,
  BuildContext context,
) {
  if (AppsPublicVariableResource.formkey.currentState!.validate()) {
    AppsPublicVariableResource.formkey.currentState!.save();
    setState(() {});
    if (AppsPublicVariableResource.username == "irfanda" &&
        AppsPublicVariableResource.password == "1234") {
      Provider.of<AppsAuthNotifier>(context, listen: false).saveUserLogin(
          AppsAuthProvider(
              username: AppsPublicVariableResource.username,
              password: AppsPublicVariableResource.password));

      print("username user provider ${AppsPublicVariableResource.username}");
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const AppsBottomNavbarWidget()));
      appsClearDataVariable(context, setState);
    }
  }
}
