//-> bottom sheet widget

import 'package:flutter/material.dart';

void appsstatisBottomSheet(
  BuildContext context,
  listButton,
) {
  final cardColor = Theme.of(context).cardColor;
  showModalBottomSheet(
      elevation: 10,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (ctx) => Padding(
            padding: (MediaQuery.of(context).size.width < 700 &&
                    MediaQuery.of(context).size.width > 515)
                ? const EdgeInsets.symmetric(horizontal: 60)
                : (MediaQuery.of(context).size.width > 700)
                    ? EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width / 7)
                    : EdgeInsets.zero,
            child: Container(
              decoration: BoxDecoration(
                  color: cardColor,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(18),
                      topRight: Radius.circular(18))),
              height: 130,
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: listButton,
                  ),
                ],
              ),
            ),
          ));
}
