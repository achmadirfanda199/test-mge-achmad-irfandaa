// apps porfil user page

import 'dart:io';
import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

class AppsProfilUser extends StatefulWidget {
  const AppsProfilUser({super.key});

  @override
  State<AppsProfilUser> createState() => _AppsProfilUserState();
}

class _AppsProfilUserState extends State<AppsProfilUser> {
//-> funcation get image from camera
  Future getImageCamera() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.camera);
    File file = File(image!.path);
    setState(() {
      AppsPublicVariableResource.gambarfile = file;
    });
  }

  //-> function get image from galery
  Future getImageGallery() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    File file = File(image!.path);
    setState(() {
      AppsPublicVariableResource.gambarfile = file;
    });
  }

  @override
  void didChangeDependencies() {
    if (!AppsPublicVariableResource.isLoad) {
      AppsPublicVariableResource.isLoad = true;
      AppsPublicVariableResource.usernameControllerProfil.text =
          AppsPublicVariableResource.usernameprofil;
      AppsPublicVariableResource.phoneControllerProfil.text =
          AppsPublicVariableResource.phoneprofil;
      AppsPublicVariableResource.addressControllerProfil.text =
          AppsPublicVariableResource.addressprofil;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return AppsScaffold(
        withfloating: false,
        withAction: true,
        withLeading: false,
        withappbar: true,
        titleAppbar: "Welcome ${AppsPublicVariableResource.username}",
        listAction: [
          AppsIconWidget(
            iconType: IconType.materialIconButton,
            uriIcon: Icons.logout_rounded,
            onPressIconButton: () {
              Provider.of<AppsAuthNotifier>(context, listen: false)
                  .userLogOut();
              appsSaveProfileFunction(context, setState);
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => const AppsLoginPage()),
                  (route) => false);
            },
          )
        ],
        mainChildWidget: Container(
            height: mediaQuery.height,
            width: mediaQuery.width,
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: SingleChildScrollView(
              padding: const EdgeInsets.only(bottom: 500),
              child: Column(
                children: [
                  Center(
                      child: Container(
                          margin: const EdgeInsets.only(top: 16, bottom: 8),
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: Theme.of(context).primaryColor,
                                width: 2),
                            shape: BoxShape.circle,
                            color: Colors.grey,
                          ),
                          child: (AppsPublicVariableResource.gambarfile != null)
                              ? Stack(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: Image.file(
                                        AppsPublicVariableResource.gambarfile!,
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    Positioned(
                                        bottom: 0,
                                        right: 0,
                                        child: AppsIconWidget(
                                          iconType: IconType.materialIconButton,
                                          uriIcon: Icons.delete_forever,
                                          colorIcon:
                                              Colors.red,
                                              sizeIcon: 30.0,
                                          onPressIconButton: () {
                                            setState(() {
                                              AppsPublicVariableResource
                                                  .gambarfile = null;
                                            });
                                          },
                                        ))
                                  ],
                                )
                              : AppsIconWidget(
                                  iconType: IconType.materialIconButton,
                                  uriIcon: Icons.add_a_photo_outlined,
                                  onPressIconButton: () {
                                    appsstatisBottomSheet(context, [
                                      AppsButtonWidget(
                                          titleButton: "Gallery",
                                          buttonType: ButtonType.normalButton,
                                          navigatorButton: () {
                                            Navigator.of(context).pop();
                                            getImageGallery();
                                          }),
                                      const SizedBox(height: 8),
                                      AppsButtonWidget(
                                          titleButton: "Camera",
                                          buttonType: ButtonType.normalButton,
                                          navigatorButton: () {
                                            Navigator.of(context).pop();
                                            getImageCamera();
                                          }),
                                    ]);
                                  },
                                ))),
                  AppsPublicVariableResource.jarakheight32,
                  AppsTextFieldWidget(
                      controller:
                          AppsPublicVariableResource.usernameControllerProfil,
                      jenisField: JenisField.universal,
                      label: "Username",
                      hintText: "username",
                      namaField: "username",
                      maxLines: 1,
                      keyboardType: TextInputType.text,
                      suffixIcon: Icon(
                        Icons.person,
                        color: Theme.of(context).primaryColor,
                      ),
                      onChanged: (value) {},
                      onsaved: (value) {
                        AppsPublicVariableResource.usernameprofil = value;
                      }),
                  AppsPublicVariableResource.jarakheight24,
                  AppsTextFieldWidget(
                      controller:
                          AppsPublicVariableResource.phoneControllerProfil,
                      jenisField: JenisField.universal,
                      label: "Phone",
                      hintText: "+62 6892 8990",
                      namaField: "Phone",
                      maxLines: 1,
                      keyboardType: TextInputType.phone,
                      suffixIcon: Icon(
                        Icons.phone,
                        color: Theme.of(context).primaryColor,
                      ),
                      onChanged: (value) {},
                      onsaved: (value) {
                        AppsPublicVariableResource.phoneprofil = value;
                      }),
                  AppsPublicVariableResource.jarakheight24,
                  AppsTextFieldWidget(
                      controller:
                          AppsPublicVariableResource.addressControllerProfil,
                      jenisField: JenisField.universal,
                      label: "Address",
                      hintText: "Surabaya, Indonesia",
                      namaField: "Address",
                      maxLines: 1,
                      keyboardType: TextInputType.streetAddress,
                      suffixIcon: Icon(
                        Icons.home,
                        color: Theme.of(context).primaryColor,
                      ),
                      onChanged: (value) {},
                      onsaved: (value) {
                        AppsPublicVariableResource.addressprofil = value;
                      }),
                ],
              ),
            )));
  }
}
