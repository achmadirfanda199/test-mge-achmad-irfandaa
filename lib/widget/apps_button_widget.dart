// apps common button

// ignore_for_file: deprecated_member_use
import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

enum ButtonType { bigButton, normalButton, textButton, widgetButton }

class AppsButtonWidget extends StatelessWidget {
  final ButtonType buttonType;
  final dynamic titleButton;
  final Function() navigatorButton;
  final dynamic customTextStyleButton;
  final dynamic backgroundColorButton;
  final dynamic childWidgetButton;

  const AppsButtonWidget({
    super.key,
    required this.buttonType,
    this.titleButton,
    required this.navigatorButton,
    this.customTextStyleButton,
    this.backgroundColorButton,
    this.childWidgetButton,
  });

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;

    final styeleDialogButton = ElevatedButton.styleFrom(
        minimumSize: const Size.fromHeight(20),
        backgroundColor: backgroundColorButton);

    final styleNormalButton = ElevatedButton.styleFrom(
        minimumSize: const Size.fromHeight(47),
        backgroundColor: backgroundColorButton,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
          side: BorderSide(color: primaryColor, width: 1),
        ),
        elevation: 0);

    final styleBigButton = ElevatedButton.styleFrom(
        minimumSize: const Size.fromHeight(70),
        backgroundColor: backgroundColorButton,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(color: primaryColor, width: 1),
        ),
        elevation: 0);

    final textthemeButton = Theme.of(context).textTheme.button;

    return (buttonType == ButtonType.textButton)
        ? ElevatedButton(
            style: styeleDialogButton,
            onPressed: navigatorButton,
            child: Text(
              titleButton,
              style: (customTextStyleButton == null)
                  ? textthemeButton!.copyWith(fontSize: 12, color: primaryColor)
                  : customTextStyleButton,
            ))
        : (buttonType == ButtonType.normalButton)
            ? ElevatedButton(
                style: styleNormalButton,
                onPressed: navigatorButton,
                child: Text(
                  titleButton,
                  style: (customTextStyleButton == null)
                      ? textthemeButton!.copyWith(
                          color: Theme.of(context).scaffoldBackgroundColor,
                          fontSize: 20)
                      : customTextStyleButton,
                ))
            : (buttonType == ButtonType.widgetButton)
                ? ElevatedButton(
                    onPressed: navigatorButton,
                    child: childWidgetButton,
                  )
                : ElevatedButton(
                    style: styleBigButton,
                    onPressed: navigatorButton,
                    child: Text(
                      titleButton,
                      style: (customTextStyleButton == null)
                          ? textthemeButton!.copyWith(
                              fontSize: 26,
                              color: primaryColor,
                              fontWeight: FontWeight.w500)
                          : customTextStyleButton,
                    ));
  }
}
