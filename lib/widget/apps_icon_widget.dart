// app icon
import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

enum IconType { imageIconButton, materialIconButton, materialIcon }

class AppsIconWidget extends StatelessWidget {
  final dynamic colorIcon;
  final IconType iconType;
  final dynamic uriIcon;
  final dynamic sizeIcon;
  final dynamic onPressIconButton;
  const AppsIconWidget({
    super.key,
    this.colorIcon,
    required this.iconType,
    this.uriIcon,
    this.sizeIcon, 
    this.onPressIconButton,
  });

  @override
  Widget build(BuildContext context) {
    return (iconType == IconType.imageIconButton)
        ? IconButton(
            onPressed: onPressIconButton,
            icon: ImageIcon(
              uriIcon,
              size: sizeIcon,
            ))
        : (iconType == IconType.materialIconButton)
            ? IconButton(
                onPressed: onPressIconButton,
                icon: Icon(
                  uriIcon,
                  size: (sizeIcon == null) ? 24 : sizeIcon,
                  color: colorIcon,
                ))
            : Icon(
                uriIcon,
                size: (sizeIcon == null) ? 24 : sizeIcon,
                color: colorIcon,
              );
  }
}
