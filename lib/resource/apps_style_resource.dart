// apps style
// ignore_for_file: deprecated_member_use

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

class AppsStylesResource {
  static ThemeData themeData(bool isDarkTheme, BuildContext context) {
    final Color allbrownshade100 = Colors.brown.shade100;

    return ThemeData(
        appBarTheme: AppBarTheme(
          titleTextStyle: Theme.of(context).textTheme.headline6!.copyWith(
                color: Colors.brown.shade200,
              ),
          elevation: 6.0,
        ),
        scaffoldBackgroundColor:Colors.white,
        primaryColor: Colors.lightBlue, 

        textTheme: TextTheme(
          headline1: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 27,
              fontWeight: FontWeight.w700),
          headline2: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 21,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.15),
          headline3: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 19,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.15),
          bodyText1: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.5),
          bodyText2: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.5),
          subtitle1: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w400,
              letterSpacing: 0.25),
          subtitle2: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 13,
              fontWeight: FontWeight.w400,
              letterSpacing: 0.25),
          button: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 21,
              fontWeight: FontWeight.w600,
              letterSpacing: 0),
          caption: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 11,
              fontWeight: FontWeight.w400,
              letterSpacing: 0.4),
        ));
  }
}
