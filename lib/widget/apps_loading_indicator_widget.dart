// apps loading indicator widget

// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';

class AppsLoadingIndicator extends StatefulWidget {
  final dynamic customValue;
  const AppsLoadingIndicator({Key? key, this.customValue}) : super(key: key);

  @override
  _AppsLoadingIndicatorState createState() => _AppsLoadingIndicatorState();
}

class _AppsLoadingIndicatorState extends State<AppsLoadingIndicator>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    )..addListener(() {
        setState(() {});
      });
    animationController.repeat();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    return Center(
      child: SizedBox(
        height: 30,
        width: 30,
        child: CircularProgressIndicator(
          value: widget.customValue,
          valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
        ),
      ),
    );
  }
}
