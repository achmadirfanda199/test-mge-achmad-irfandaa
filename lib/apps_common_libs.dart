// apps common libs

export 'package:flutter/material.dart';
export 'package:provider/provider.dart';
export 'package:sqflite/sqflite.dart';
export 'package:intl/date_symbol_data_local.dart';
export 'package:google_fonts/google_fonts.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:image_picker/image_picker.dart'; 
export 'package:test_mge_achmad_irfandaa/apps_scaffold.dart';

export 'package:test_mge_achmad_irfandaa/provider/apps_theme_provider.dart';
export 'package:test_mge_achmad_irfandaa/provider/apps_auth_provider.dart';
export 'package:test_mge_achmad_irfandaa/provider/language_provider.dart';
export 'package:test_mge_achmad_irfandaa/provider/apps_profile_provider.dart';

export 'package:test_mge_achmad_irfandaa/resource/apps_style_resource.dart';
export 'package:test_mge_achmad_irfandaa/resource/apps_icon_resource.dart';
export 'package:test_mge_achmad_irfandaa/resource/apps_image_resource.dart';
export 'package:test_mge_achmad_irfandaa/resource/apps_public_variable_resource.dart';

export 'package:test_mge_achmad_irfandaa/view/auth/apps_login_page.dart';
export 'package:test_mge_achmad_irfandaa/view/apps_home_page.dart';
export 'package:test_mge_achmad_irfandaa/view/apps_profil_user_page.dart';


export 'package:test_mge_achmad_irfandaa/widget/apps_custom_message_error_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_statis_bottom_sheet_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_main_login_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_appbar_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_icon_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_divider_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_button_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_textfield_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_bottom_navbar_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_loading_indicator_widget.dart';
export 'package:test_mge_achmad_irfandaa/widget/apps_pop_up_dialog_widget.dart';

export 'package:test_mge_achmad_irfandaa/function/apps_login_function.dart';
export 'package:test_mge_achmad_irfandaa/function/apps_clear_variable.dart';
export 'package:test_mge_achmad_irfandaa/function/apps_save_profile_function.dart';


export 'package:test_mge_achmad_irfandaa/model/api_model.dart';