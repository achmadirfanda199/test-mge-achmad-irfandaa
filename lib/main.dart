// ignore_for_file: deprecated_member_use

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AppsAuthNotifier>(
              create: (_) => AppsAuthNotifier()),
          ChangeNotifierProvider<AppsProfilNotifier>(
              create: (_) => AppsProfilNotifier())
        ],
        child: GestureDetector(
            onTap: () {
              FocusScopeNode currentfocus = FocusScope.of(context);
              if (!currentfocus.hasPrimaryFocus) {
                currentfocus.unfocus();
              }
            },
            child: MaterialApp(
                theme: ThemeData.light(),
                debugShowCheckedModeBanner: false,
                home: const Wrapper())));
  }
}

class Wrapper extends StatefulWidget {
  const Wrapper({super.key});

  @override
  State<Wrapper> createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  @override
  Widget build(BuildContext context) {
    var userProvider =
        Provider.of<AppsAuthNotifier>(context).appsUserDataProvider;
    setState(() {
      AppsPublicVariableResource.username = userProvider.username;
      AppsPublicVariableResource.password = userProvider.password;
    });

    if (userProvider.isLogin) {
      return const AppsBottomNavbarWidget();
    } else {
      return const AppsLoginPage();
    }
  }
}
