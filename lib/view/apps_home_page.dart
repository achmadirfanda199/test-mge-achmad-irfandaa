// apps home page

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';
import 'package:http/http.dart' as https;
import 'dart:convert';

class AppsHomePage extends StatefulWidget {
  const AppsHomePage({super.key});

  @override
  State<AppsHomePage> createState() => _AppsHomePageState();
}

class _AppsHomePageState extends State<AppsHomePage> {
  @override
  Widget build(BuildContext context) {
    final subtitle2 = Theme.of(context).textTheme.subtitle2;
    final caption = Theme.of(context).textTheme.caption;

    Future<List<APIModels>> fetchData() async {
      var url = Uri.parse('https://jsonplaceholder.typicode.com/photos');
      final response = await https.get(url);
      print("response json ${response.body}");

      if (response.statusCode == 200) {
        List jsonResponse = json.decode(response.body);
        return jsonResponse.map((data) => APIModels.fromJson(data)).toList();
      } else {
        throw Exception('Unexpected error occured!');
      }
    }

    return AppsScaffold(
        withfloating: true,
        withAction: true,
        withLeading: false,
        withappbar: true,
        navigatorfloating: () {},
        uriIconFloating: Icons.download,
        titleAppbar: "Welcome ${AppsPublicVariableResource.username}",
        listAction: [
          AppsIconWidget(
            iconType: IconType.materialIconButton,
            uriIcon: Icons.logout_rounded,
            onPressIconButton: () {
              Provider.of<AppsAuthNotifier>(context, listen: false)
                  .userLogOut();
              print("user logout");
              appsClearDataVariable(context, setState);
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => const AppsLoginPage()),
                  (route) => false);
            },
          )
        ],
        mainChildWidget: Container(
            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: FutureBuilder<List<APIModels>>(
              future: fetchData(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      itemCount: snapshot.data!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                            onTap: () {
                              customdialog(
                                  context,
                                  TypeDialog.detail,
                                  JumlahButton.satu,
                                  NavType.pop,
                                  Column(
                                    children: [
                                      Text(
                                        snapshot.data![index].title,
                                      ),
                                      Image.network(
                                          snapshot.data![index].thumbnailUrl)
                                    ],
                                  ),
                                  "Tutup", () {
                                Navigator.of(context).pop();
                              });
                            },
                            leading: SizedBox(
                                height: 70,
                                child: Image.network(
                                    snapshot.data![index].thumbnailUrl)),
                            title: Text(snapshot.data![index].title,
                                style: subtitle2,
                                textAlign: TextAlign.justify,
                                overflow: TextOverflow.ellipsis),
                            subtitle: Text("URI: ${snapshot.data![index].url}",
                                style: caption,
                                textAlign: TextAlign.justify,
                                overflow: TextOverflow.ellipsis),
                            trailing: AppsIconWidget(
                              iconType: IconType.materialIconButton,
                              uriIcon: Icons.delete,
                              onPressIconButton: () {
                                //detele pop
                                customdialog(
                                    context,
                                    TypeDialog.delete,
                                    JumlahButton.dua,
                                    NavType.custom,
                                    "Anda yakin ingin menghapus data ini?",
                                    "Delete", () {
                                  Navigator.of(context).pop();
                                  setState(() {
                                    snapshot.data!.removeAt(index);
                                  });
                                });
                              },
                            ));
                      });
                } else if (snapshot.hasError) {
                  return Center(
                      child: Text(
                    snapshot.error.toString(),
                    style: subtitle2,
                  ));
                }

                return const AppsLoadingIndicator();
              },
            )));
  }
}
