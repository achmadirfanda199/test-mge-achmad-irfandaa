// apps bottom navbar

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

class AppsBottomNavbarWidget extends StatefulWidget {
  const AppsBottomNavbarWidget({Key? key}) : super(key: key);

  @override
  State<AppsBottomNavbarWidget> createState() => _AppsBottomNavbarWidgetState();
}

class _AppsBottomNavbarWidgetState extends State<AppsBottomNavbarWidget> {
  int indexs = 0;

  void onItemTap(int index) {
    setState(() {
      indexs = index;
      if (indexs == 1){
        AppsPublicVariableResource.isLoad = true; 
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final cardColor = Colors.grey.shade400;
    final scaffoldBackgroundColor = Theme.of(context).scaffoldBackgroundColor;

    final List<Widget> option = <Widget>[
      const AppsHomePage(),
      const AppsProfilUser(),
    ];

    return Scaffold(
      body: option.elementAt(indexs),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
            border: Border(top: BorderSide(color: cardColor)),
            boxShadow: [
              BoxShadow(
                  color: primaryColor,
                  blurRadius: 0.2,
                  offset: Offset.zero,
                  spreadRadius: 0.2)
            ]),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: primaryColor,
          currentIndex: indexs,

          selectedItemColor: scaffoldBackgroundColor,
          unselectedItemColor: cardColor,
          iconSize: 24.0,
          onTap: onItemTap,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                (indexs == 0) ? Icons.list : Icons.list,
              ),
              label: "List"
            ),
            BottomNavigationBarItem(
              icon: Icon(
                (indexs == 1)
                    ? Icons.person
                    : Icons.person_2_outlined,
              ),
              label: 'Profile',
            ),
          ],
        ),
      ),
    );
  }
}
