// apps pop up dialog

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';
import 'package:test_mge_achmad_irfandaa/widget/button_dialog_widget.dart';

enum TypeDialog {
  detail,
  error,
  delete,
}

enum JumlahButton {
  satu,
  dua,
}

enum NavType {
  pop,
  push,
  pushRemoveUntil,
  custom,
}

void customdialog(
  BuildContext context,
  TypeDialog typeDialog,
  JumlahButton jumlahButton,
  NavType navigatorType,
  message,
  titleButton,
  onpresbuttoncustom,
) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      final customTitle =
          Theme.of(context).textTheme.subtitle1!.copyWith(fontSize: 17);
      final subtitle2 = Theme.of(context).textTheme.subtitle2;
      final scaffoldBackgroundColor = Theme.of(context).scaffoldBackgroundColor;
      final cardColor = Theme.of(context).cardColor;
      final primaryColor = Theme.of(context).primaryColor;

      //-> jarak antar content
      const jarak16 = SizedBox(height: 16);
      const jarak8 = SizedBox(height: 8);
      const jarak6 = SizedBox(height: 6);

      dynamic icon;
      dynamic title;

      switch (typeDialog) {
        case TypeDialog.detail:
          title = "Detail ";
          icon = Icon(
            Icons.document_scanner_outlined,
            color: primaryColor,
            size: 28,
          );

          break;
        case TypeDialog.delete:
          title = 'Delete Data';
          icon = Icon(
            Icons.delete,
            color: primaryColor,
            size: 28,
          );
          break;
        default:
          icon = const Icon(null);
      }

      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
        elevation: 5,
        backgroundColor: scaffoldBackgroundColor,
        scrollable: true,
        contentPadding: const EdgeInsets.all(4),
        content: Container(
          width: 320,
          margin: const EdgeInsets.all(2),
          decoration: BoxDecoration(
              color: cardColor, borderRadius: BorderRadius.circular(14)),
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.center,
                child: icon,
              ),
              jarak8,
              Text(
                title,
                style: customTitle,
                textAlign: TextAlign.center,
              ),
              jarak6,
              Container(
                  width: 280,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  child: (typeDialog ==TypeDialog.detail ) ?
                  message
                  :
                  Text(
                    message,
                    style: subtitle2,
                    textAlign: TextAlign.center,
                  )),
              jarak6,
              Text(
                "Terimakasih.",
                style: subtitle2,
                textAlign: TextAlign.center,
              ),
              jarak16,
              if (jumlahButton == JumlahButton.satu &&
                  navigatorType == NavType.pop)
                ButtonDialog(
                  title: (titleButton != null) ? titleButton : 'Tutup',
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  buttonType: ButtonTypeDialog.cancel,
                ),
              if (jumlahButton == JumlahButton.dua)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    if (jumlahButton == JumlahButton.dua)
                      Expanded(
                          child: ButtonDialog(
                        title: 'Batal',
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        buttonType: ButtonTypeDialog.cancel,
                      )),
                    if (jumlahButton == JumlahButton.dua)
                      const SizedBox(
                        width: 16,
                      ),
                    Expanded(
                        child: ButtonDialog(
                      title: titleButton,
                      onPressed: onpresbuttoncustom,
                      buttonType: ButtonTypeDialog.normal,
                    ))
                  ],
                )
            ],
          ),
        ),
      );
    },
  );
}
