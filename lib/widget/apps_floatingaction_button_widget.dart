// apps floating action button

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

Widget appsfloatingbuttonwidget(
  context,
  navigatorfloating,
  uriIconFloating,
  withfloating,
) {
  return (withfloating == true)
      ? FloatingActionButton(
          onPressed: navigatorfloating,
          backgroundColor: Theme.of(context).primaryColor,
          child: AppsIconWidget(
            iconType: IconType.materialIcon,
            colorIcon: Theme.of(context).scaffoldBackgroundColor,
            uriIcon: uriIconFloating,
          ),
        )
      : Container();
}
