// apps textfield

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

enum JenisField { universal, currency, email, password }

class AppsTextFieldWidget extends StatefulWidget {
  final TextEditingController controller;
  final JenisField jenisField;
  final String label;
  final String hintText;
  final String namaField;
  final int maxLines;
  final TextInputType keyboardType;
  //
  final dynamic prefixIcon;
  final dynamic prefixText;
  final dynamic prefixTextStyle;
  final dynamic suffixIcon;
  final dynamic suffixText;
  final dynamic suffixTextStyle;
  //
  final Function(dynamic) onChanged;
  final Function(dynamic) onsaved;

  const AppsTextFieldWidget({
    super.key,
    required this.controller,
    required this.jenisField,
    required this.label,
    required this.hintText,
    required this.namaField,
    required this.maxLines,
    required this.keyboardType,
    //
    this.prefixIcon,
    this.prefixText,
    this.prefixTextStyle,
    this.suffixIcon,
    this.suffixText,
    this.suffixTextStyle,
    //
    required this.onChanged,
    required this.onsaved,
  });

  @override
  State<AppsTextFieldWidget> createState() => _AppsTextFieldWidgetState();
}

class _AppsTextFieldWidgetState extends State<AppsTextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    final bodyText1 = Theme.of(context).textTheme.bodyText1;
    final hoverColor = Theme.of(context).primaryColor;
    const disabledColor = Colors.black;

    return TextFormField(
      controller: widget.controller,
      style: bodyText1,
      enabled: true,
      maxLines: widget.maxLines,
      textAlign: TextAlign.justify,
      keyboardType: widget.keyboardType,
      obscureText: (widget.jenisField == JenisField.password)
          ? AppsPublicVariableResource.obscureText
          : false,
      decoration: InputDecoration(
        //prefix
        prefixIcon: widget.prefixIcon,
        prefixText: widget.prefixText,
        prefixStyle: widget.prefixTextStyle,
        //suffix
        suffixIcon: widget.suffixIcon,
        suffixText: widget.suffixText,
        suffixStyle: widget.suffixTextStyle,
        //
        hoverColor: hoverColor,
        labelText: widget.label,
        labelStyle: bodyText1,
        hintText: widget.hintText,
        hintStyle: bodyText1!
            .copyWith(color: const Color.fromARGB(255, 186, 186, 186)),
        contentPadding: const EdgeInsets.only(left: 20, top: 12, bottom: 12),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: hoverColor)),
        disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: const BorderSide(color: disabledColor)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: hoverColor)),
      ),
      onChanged: (value) {
        widget.onChanged(value);
      },
      onSaved: (value) {
        widget.onsaved(value);
      },
    );
  }
}
