//-> custom button dialog
// ignore_for_file: deprecated_member_use
import "package:flutter/material.dart";

typedef OnPressed = void Function(); //-> define type

enum ButtonTypeDialog {
  cancel,
  normal,
  diatasNormal,
}

class ButtonDialog extends StatelessWidget {
  const ButtonDialog({
    required this.title,
    required this.onPressed,
    required this.buttonType,
    Key? key,
  }) : super(key: key);

  final String title;
  final OnPressed onPressed;
  final ButtonTypeDialog buttonType;

  @override
  Widget build(BuildContext context) {
    final customCancelColor = Colors.grey.shade500;
    final bgColorDiatasNormal = Colors.purple.shade50;
    const textColorDiatasNormal = Colors.black;
    final scaffoldBackgroundColor = Theme.of(context).scaffoldBackgroundColor;
    final primaryColor = Theme.of(context).primaryColor;
    final bodyText1 = Theme.of(context).textTheme.bodyText1;
    final subtitle2 = Theme.of(context).textTheme.subtitle2;

    //->texttheme
    final styleText = (buttonType == ButtonTypeDialog.diatasNormal)
        ? bodyText1?.copyWith(color: textColorDiatasNormal)
        : subtitle2?.copyWith(
            color: scaffoldBackgroundColor, fontWeight: FontWeight.w800);

    return TextButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(vertical: 0, horizontal: 10)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12))),
          backgroundColor: (buttonType == ButtonTypeDialog.normal) //-> kondisi 1
              ? MaterialStateProperty.all(primaryColor)
              : (buttonType == ButtonTypeDialog.diatasNormal) //-> kondisi 2
                  ? MaterialStateProperty.all(bgColorDiatasNormal)
                  : MaterialStateProperty.all(customCancelColor),
        ),
        onPressed: onPressed,
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: (buttonType == ButtonTypeDialog.diatasNormal) ? 10 : 4),
          child: Text(
            title,
            style: styleText,
          ),
        ));
  }
}
