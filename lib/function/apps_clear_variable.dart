// app function clear data variable

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

void appsClearDataVariable(context, setState) {
  setState(() {
    AppsPublicVariableResource.usernameController.clear();
    AppsPublicVariableResource.passwordController.clear();
  });
}
