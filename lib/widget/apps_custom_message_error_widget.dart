// apps custom message error

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

enum ErrorFor { username, password }

Widget appsCustomMessageError(context, ErrorFor errorFor) {
  return Align(
    alignment: Alignment.topLeft,
    child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 22),
        child: Text(
          (errorFor == ErrorFor.password)
              ? "Password tidak cocok"
              : "Username tidak ditemukan",
          style:
              Theme.of(context).textTheme.caption!.copyWith(color: Colors.red),
        )),
  );
}
