// function save state

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

void appsSaveProfileFunction(context, setState) async {
  if (AppsPublicVariableResource.formkey.currentState!.validate()) {
    AppsPublicVariableResource.formkey.currentState!.save();

    Provider.of<AppsProfilNotifier>(context, listen: false)
        .saveDataProfile(AppsProfileProvider(
      usernameprofil: AppsPublicVariableResource.usernameprofil,
      phoneprofil: AppsPublicVariableResource.phoneprofil,
      addressprofil: AppsPublicVariableResource.addressprofil,
      gambar: AppsPublicVariableResource.gambarfile.toString(),
    ));
  }
}
