// apps login page

// ignore_for_file: deprecated_member_use

import 'package:test_mge_achmad_irfandaa/apps_common_libs.dart';

class AppsLoginPage extends StatefulWidget {
  const AppsLoginPage({super.key});

  @override
  State<AppsLoginPage> createState() => _AppsLoginPageState();
}

class _AppsLoginPageState extends State<AppsLoginPage> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;

    return AppsScaffold(
      withfloating: false,
      withappbar: false,
      withLeading: false,
      withAction: false,
      mainChildWidget: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SizedBox(
              width: mediaQuery.width,
              child: const Image(
                image: AssetImage(
                  AppsImageResource.login,
                ),
                fit: BoxFit.fitWidth,
              )),
          Container(
            height: 450,
            padding: const EdgeInsets.symmetric(
              horizontal: 32,
            ),
            child: const SingleChildScrollView(
                padding: EdgeInsets.only(bottom: 500),
                child: AppMainLoginWidget()),
          ),
        ],
      ),
    );
  }
}
